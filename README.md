# **Description**
This module verifies that a password conforms to construction rules.

# Usage
    let ruler = require( "./ruler.js" );
    console.log( ruler.acceptPassword( "foobar" ) ); // false

    let criteria = {
       minLength: 6,
       minUpper: 0,
       minDigit: 0,
       minSpecial: 0
    };
    console.log( ruler.acceptPassword( "foobar", criteria ); // true

# API Documentation

## **acceptPassword( password [, criteria] [, callback] )**
Checks a password against a set of criteria to see if it is acceptable.

### Arguments
- password: [string] The password to check.
- critiera: [optional object] Set of criteria to check the password against.
    - minLength: [number] Minimum acceptable length.  Default is 1.
    - maxLength: [number] Maximum acceptable length.  Default is 1.
    - minUpper: [number] Minimum number of upper case letters required.  Default is 1.
    - minLower: [number] Minimum number of lower case letters required.  Default is 1.
    - minDigit: [number] Minimum number of digits required.  Default is 1.
    - minSpecial: [number] Minimum number of special characters required.  Default is 1.
    - specials: [string] String containing acceptable special characters.  Default is "!@#$%^&*()_-+={[}]|\\:;\"\'<,>.?/".
    - excluded: [string] String containing characters that cannot be in the password.  Default is "".
- callback: [optional function] Function called when the operation completes.
    - err: Error object containing information on why the password was rejected.  Will be null if no error.

### Returns
True if the password is acceptable, false if it was rejected.