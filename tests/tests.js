var ruler = require( "../src/ruler" );

exports.testAcceptFailNull = function( test ) {
   test.ok( !ruler.acceptPassword( null ) );
   test.done();
};

exports.testAcceptFailEmpty = function( test ) {
   test.ok( !ruler.acceptPassword( "" ) );
   test.done();
};

exports.testAcceptFailMinLength = function( test ) {
   var password = "fooBar";
   test.ok( !ruler.acceptPassword( password ) );
   test.done();
};

exports.testAcceptPassMinLength = function( test ) {
   var password = "!foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, null, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptPassSetMinLengthZero = function( test ) {
   var password = "!foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, {minLength: 0}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptPassSetMinLengthNegative = function( test ) {
   var password = "!foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, {minLength: -1}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailMaxLength = function( test ) {
   var password = "foobaRfoobarfoobarfoobarfoobarfoobarf";
   test.ok( !ruler.acceptPassword( password ) );
   test.done();
};

exports.testAcceptPassMaxLength = function( test ) {
   var password = "!foobarfOobarfoobarfoobarfoobar1";
   test.ok( ruler.acceptPassword( password, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailSetMinLength = function( test ) {
   var password = "#foobAr";
   test.ok( !ruler.acceptPassword( password, {minLength: password.length + 1} ) );
   test.done();
};

exports.testAcceptPassSetMinLength = function( test ) {
   var password = "!Foobar1";
   test.ok( ruler.acceptPassword( password, {minLength: password.length}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailSetMaxLength = function( test ) {
   var password = "f%oobarFoobarfoobar";
   test.ok( !ruler.acceptPassword( password, {maxLength: password.length - 1} ) );
   test.done();
};

exports.testAcceptPassSetMaxLength = function( test ) {
   var password = "!foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, {maxLength: password.length}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptPassSetMaxLengthZero = function( test ) {
   var password = "!foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, {maxLength: 0}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptPassSetMaxLengthNegative = function( test ) {
   var password = "!foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, {maxLength: -1}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailMinUpper = function( test ) {
   var password = "foob@rfoobarfoobar";
   test.ok( !ruler.acceptPassword( password ) );
   test.done();
};

exports.testAcceptPassMinUpper = function( test ) {
   var password = "!foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailMinLower = function( test ) {
   var password = "FOOBARFOOBAR&FOOBAR";
   test.ok( !ruler.acceptPassword( password ) );
   test.done();
};

exports.testAcceptPassMinLower = function( test ) {
   var password = "!FOOBARFOOBARFOOBAr1";
   test.ok( ruler.acceptPassword( password, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailSetMinLower = function( test ) {
   var password = "FOOBARFOOBARFOOBAr1+";
   test.ok( !ruler.acceptPassword( password, {minLower: 2} ) );
   test.done();
};

exports.testAcceptPassSetMinLower = function( test ) {
   var password = "!FOOBARfOOBARFOOBAr1";
   test.ok( ruler.acceptPassword( password, {minLower: 2}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   password = "!FOOBArfOOBARFOOBAR1";
   test.ok( ruler.acceptPassword( password, {minLower: 2}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailMinDigit = function( test ) {
   var password = "foobarFoobarfoo*ar";
   test.ok( !ruler.acceptPassword( password ) );
   test.done();
};

exports.testAcceptPassMinDigit = function( test ) {
   var password = "^foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailSetMinDigit = function( test ) {
   var password = "foobarFoobarfoobar1$";
   test.ok( !ruler.acceptPassword( password, {minDigit: 2} ) );
   test.done();
};

exports.testAcceptPassSetMinDigit = function( test ) {
   var password = "\\foobarFoobarfoobar12";
   test.ok( ruler.acceptPassword( password, {minDigit: 2}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailMinSpecial = function( test ) {
   var password = "foobarFoobarfoobar1";
   test.ok( !ruler.acceptPassword( password ) );
   test.done();
};

exports.testAcceptPassMinSpecial = function( test ) {
   var password = "?foobarFoobarfoobar1";
   test.ok( ruler.acceptPassword( password, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailSetMinSpecial = function( test ) {
   var password = "foobarFoobarfoobar1";
   test.ok( !ruler.acceptPassword( password, {minSpecial: 2} ) );
   test.done();
};

exports.testAcceptPassSetMinSpecial = function( test ) {
   var password = "?foobarFoobarfoobar1]";
   test.ok( ruler.acceptPassword( password, {minSpecial: 2}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailSetSpecials = function( test ) {
   var password = "foobarFoobarfoobar1@";
   test.ok( !ruler.acceptPassword( password, {specials: "!#$%^&*()_+-="} ) );
   test.done();
};

exports.testAcceptPassSetSpecials = function( test ) {
   var password = "foobarFoobarfoobar1~";
   test.ok( ruler.acceptPassword( password, {specials: "~"}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailEmptySpecialsWithMinSpecial = function( test ) {
   var password = "foobarFoobarfoobar1!";
   test.ok( !ruler.acceptPassword( password, {minSpecial: 1, specials: ""} ) );
   test.done();
};

exports.testAcceptPassEmptySpecialsWithZeroMinSpecial = function( test ) {
   var password = "foobarFoobarfoobar1!";
   test.ok( ruler.acceptPassword( password, {minSpecial: 0, specials: ""}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptPassSetMinLowerZero = function( test ) {
   var password = "FOOBARFOOBARFOOBAR1!";
   test.ok( ruler.acceptPassword( password, {minLower: 0}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailSetMinLowerNegative = function( test ) {
   var password = "FOOBARFOOBARFOOBAR1!";
   test.ok( !ruler.acceptPassword( password, {minLower: -1} ) );
   test.done();
};

exports.testAcceptPassSetMinUpperZero = function( test ) {
   var password = "foobarfoobarfoobar1!";
   test.ok( ruler.acceptPassword( password, {minUpper: 0}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};

exports.testAcceptFailSetMinUpperNegative = function( test ) {
   var password = "foobarfoobarfoobar1!";
   test.ok( !ruler.acceptPassword( password, {minUpper: -1} ) );
   test.done();
};

exports.testAcceptFailExcluded = function( test ) {
   var password = "!FoobarfooBarfoobaR1~";
   test.ok( !ruler.acceptPassword( password, {excluded: "~"} ) );
   test.done();
};

exports.testAcceptPassExcluded = function( test ) {
   var password = "!FoobarfooBarfoobaR1";
   test.ok( ruler.acceptPassword( password, {excluded: "~"}, function( err ) {
      if ( err ) {
         console.log( err );
      }
   } ) );
   test.done();
};
