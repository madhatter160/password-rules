var Ruler = function() {
};

Ruler.prototype.acceptPassword = function( password, criteria, callback ) {
   "use strict";
   var defaultCriteria = {
      minLength: 16,
      maxLength: 32,
      minUpper: 1,
      minLower: 1,
      minDigit: 1,
      minSpecial: 1,
      specials: "!@#$%^&*()_-+={[}]|\\:;\"\'<,>.?/",
      excluded: ""
   };

   var regExEscape = function( regexPattern ) {
      "use strict";
      var escaped = "";
      var metacharacters = "\\^$.|?*+()[]{-";
      for ( var i = 0; i < regexPattern.length; i++ ) {
         var c = regexPattern[i];
         if ( 0 <= metacharacters.indexOf( c ) ) {
            escaped += "\\" + c;
         }
         else {
            escaped += c;
         }
      }

      return escaped;
   };

   if ( !criteria ) {
      criteria = defaultCriteria;
   }
   else {
      if ( !criteria.minLength ) {
         criteria.minLength = defaultCriteria.minLength;
      }
      if ( !criteria.maxLength ) {
         criteria.maxLength = defaultCriteria.maxLength;
      }
      if ( !criteria.minUpper && ( criteria.minUpper !== 0 ) ) {
         criteria.minUpper = defaultCriteria.minUpper;
      }
      if ( !criteria.minLower && ( criteria.minLower !== 0 ) ) {
         criteria.minLower = defaultCriteria.minLower;
      }
      if ( !criteria.minDigit && ( criteria.minDigit !== 0 ) ) {
         criteria.minDigit = defaultCriteria.minDigit;
      }
      if ( !criteria.minSpecial && ( criteria.minSpecial !== 0 ) ) {
         criteria.minSpecial = defaultCriteria.minSpecial;
      }
      if ( !criteria.specials && ( criteria.specials !== "" ) ) {
         criteria.specials = defaultCriteria.specials;
      }
      if ( !criteria.excluded && ( criteria.excluded !== "" ) ) {
         criteria.excluded = defaultCriteria.excluded;
      }
   }

   if ( !password ) {
      if ( callback ) {
         callback( new Error( "Password cannot be null or empty." ), false );
      }
      return false;
   }

   if ( criteria.maxLength > 0 ) {
      if ( criteria.minLength > criteria.maxLength ) {
         if ( callback ) {
            callback( new Error( "The maximum length of a password must be greater than the minimum length." ), false );
         }
         return false;
      }

      if ( password.length > criteria.maxLength ) {
         if ( callback ) {
            callback( new Error( "Password can only have a maximum of " + criteria.minLength + " characters." ), false );
         }
         return false;
      }
   }

   if ( criteria.minLower < 0 ) {
      if ( callback ) {
         callback( new Error( "Cannot have a minimum lower case character value that is less than zero." ), false );
      }
      return false;
   }

   if ( criteria.minUpper < 0 ) {
      if ( callback ) {
         callback( new Error( "Cannot have a minimum upper case character value that is less than zero." ), false );
      }
      return false;
   }

   if ( password.length < criteria.minLength ) {
      if ( callback ) {
         callback( new Error( "Password must be at least " + criteria.minLength + " characters long." ), false );
      }
      return false;
   }

   var matches;

   matches = password.match( /[A-Z]/g );
   if ( ( matches && ( matches.length < criteria.minUpper ) ) || ( !matches && criteria.minUpper > 0 ) ) {
      if ( callback ) {
         callback( new Error( "Must have at least " + criteria.minUpper + " upper case letters." ), false );
      }
      return false;
   }

   matches = password.match( /[a-z]/g );
   if ( ( matches && ( matches.length < criteria.minLower ) ) || ( !matches && criteria.minLower > 0 ) ) {
      if ( callback ) {
         callback( new Error( "Must have at least " + criteria.minLower + " lower case letters." ), false );
      }
      return false;
   }

   matches = password.match( /[0-9]/g );
   if ( ( matches && ( matches.length < criteria.minDigit ) ) || ( !matches && criteria.minDigit > 0 ) ) {
      if ( callback ) {
         callback( new Error( "Must have at least " + criteria.minDigit + " digits." ), false );
      }
      return false;
   }

   if ( criteria.specials ) {
      if ( criteria.specials.length > 0 ) {
         var escapedSpecials = regExEscape( criteria.specials );
         matches = password.match( new RegExp( "[" + escapedSpecials + "]", "g" ) );
         if ( ( matches && ( matches.length < criteria.minSpecial ) ) || ( !matches && criteria.minSpecial > 0 ) ) {
            if ( callback ) {
               callback( new Error( "Must have at least " + criteria.minSpecial + " special characters." ), false );
            }
            return false;
         }
      }
   }
   else {
      if ( criteria.minSpecial > 0 ) {
         if ( callback ) {
            callback( new Error( "No special characters designated, but a minimum number of special characters was specified." ), false );
         }
         return false;
      }
   }

   if ( criteria.excluded ) {
      if ( criteria.excluded.length > 0 ) {
         var escapedExcluded = regExEscape( criteria.excluded );
         matches = password.match( new RegExp( "[" + escapedExcluded + "]", "g" ) );
         if ( matches ) {
            if ( callback ) {
               callback( new Error( "Password cannot contain excluded characters." ) );
            }
            return false;
         }
      }
   }

   if ( callback ) {
      callback( null, true );
   }

   return true;
};

module.exports = new Ruler();
